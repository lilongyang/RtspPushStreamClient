package com.lly.controller;

import com.lly.entry.TaskInfo;
import com.lly.thread.FFmpegRunner;
import com.lly.thread.FFmpegThreadPoolExecutor;
import com.lly.view.AddTask;
import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

@FXMLController
public class FileExplorerController implements Initializable {
    private static Logger logger = LoggerFactory.getLogger(FileExplorerController.class);
    private ObservableList<TaskInfo> taskInfoObservableList = FXCollections.observableList(new ArrayList<>());
    @FXML
    private TableView<TaskInfo> table;
    @FXML
    private TableColumn<TaskInfo, String> taskNameCol;
    @FXML
    private TableColumn<TaskInfo, Integer> taskStatusCol;
    @FXML
    private TableColumn<TaskInfo, String> inPathCol;
    @FXML
    private TableColumn<TaskInfo,String> inMediaTypeCol;
    @FXML
    private TableColumn<TaskInfo,Integer> inBitCol;
    @FXML
    private TableColumn<TaskInfo,String> outPathCol;
    @FXML
    private TableColumn<TaskInfo,String> outMediaTypeCol;
    @FXML
    private TableColumn<TaskInfo,Integer> outBitCol;
    @FXML
    private TableColumn<TaskInfo, Integer> bitRateCol;
    @FXML
    private TableColumn<TaskInfo, ProgressBar> progressCol;
    @FXML
    private TableColumn<TaskInfo, String> remarkCol;

    private AnchorPane addTaskPane;
    @FXML
    private TextArea loggerTextArea;
    @Autowired
    private AddTaskController addTaskController;
    @Autowired
    private AddTask addTask;

    private ObservableList<TaskInfo> taskTableData = FXCollections.observableArrayList();

    public FileExplorerController() {

    }

    @FXML
    void openAddTaskForm(ActionEvent actionEvent) {
        addTask.showViewAndWait(Modality.APPLICATION_MODAL);
        TaskInfo taskInfo = addTaskController.getTaskInfo();
        if (taskInfo != null) {
            taskInfoObservableList.add(taskInfo);
        }
    }

    @FXML
    void openEditTaskForm(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "编辑页面");
        alert.showAndWait();
    }


    @FXML
    void addTable(ActionEvent event) {
        logger.info("demo");

//        TaskInfo task = new TaskInfo("任务1",  "rtsp://admin:Jy123456@192.168.31.153/LIveMedia/ch1/Media1", "rtmp://192.168.31.230:1935/live/huaweiipc153", 100, "备注");
//        customerModel.addTaskInfo(task);
//        TaskInfo task2 = new TaskInfo("任务2",  "rtsp://admin:Jy123456@192.168.31.154/LIveMedia/ch1/Media1", "rtmp://192.168.31.230:1935/live/huaweiipc154", 100,  "备注");
//        customerModel.addTaskInfo(task2);
//        TaskInfo task3 = new TaskInfo("任务3", "rtsp://admin:Jy123456@192.168.31.155/LIveMedia/ch1/Media1", "rtmp://192.168.31.230:1935/live/huaweiipc155", 100, "备注");
//        customerModel.addTaskInfo(task3);
//        TaskInfo task4 = new TaskInfo("任务4", "rtsp://admin:Jy123456@192.168.31.153/LIveMedia/ch1/Media2", "rtmp://192.168.31.230:1935/live/huaweiipc1551", 100, "备注");
//        customerModel.addTaskInfo(task4);
//        TaskInfo task5 = new TaskInfo("任务5", "rtsp://admin:Jy123456@192.168.31.154/LIveMedia/ch1/Media1", "rtmp://192.168.31.230:1935/live/huaweiipc1552", 100, "备注");
//        customerModel.addTaskInfo(task5);
//        TaskInfo task6 = new TaskInfo("任务6", "rtsp://admin:Jy123456@192.168.31.154/LIveMedia/ch1/Media1", "rtmp://192.168.31.230:1935/live/huaweiipc1553", 100, "备注");
//        customerModel.addTaskInfo(task6);
//        TaskInfo task7 = new TaskInfo("任务7", "rtsp://admin:Jy123456@192.168.31.154/LIveMedia/ch1/Media1", "rtmp://192.168.31.230:1935/live/huaweiipc1554", 100, "备注");
//        customerModel.addTaskInfo(task7);


        showTableData();
    }



    public void showTableData() {
//        DataUtil.write(taskTableData);
//        if (table == null){
//            table = new TableView();
//        }
        taskNameCol.setCellValueFactory(new PropertyValueFactory<TaskInfo,String>("taskName"));
        taskStatusCol.setCellFactory(col->{
            TableCell<TaskInfo, Integer> cell = new TableCell<TaskInfo, Integer>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty) {
                        int rowIndex = this.getIndex();
                        int status = table.getItems().get(rowIndex).getTaskStatus();
                        String statusStr = "";
                        if (0 == status) {
                            statusStr = "未推送";
                        } else if (1 == status){
                            statusStr = "推送中";
                        }
                        this.setText(statusStr);
                    }
                }

            };
            return cell;
        });
        inPathCol.setCellValueFactory(new PropertyValueFactory<TaskInfo,String>("inPath"));
//        inMediaTypeCol.setCellValueFactory(new PropertyValueFactory<TaskInfo,String>("inMediaType"));
//        inBitCol.setCellValueFactory(new PropertyValueFactory<TaskInfo, Integer>("inBit"));
        outPathCol.setCellValueFactory(new PropertyValueFactory<TaskInfo,String>("outPath"));
//        outMediaTypeCol.setCellValueFactory(new PropertyValueFactory<TaskInfo,String>("outMediaType"));
//        outBitCol.setCellValueFactory(new PropertyValueFactory<TaskInfo,Integer>("outBit"));
        remarkCol.setCellValueFactory(new PropertyValueFactory<TaskInfo,String>("remark"));
        progressCol.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<TaskInfo, ProgressBar>, ObservableValue<ProgressBar>>() {
                    @Override
                    public ObservableValue<ProgressBar> call(
                            TableColumn.CellDataFeatures<TaskInfo, ProgressBar> param) {
                        // 取出 table 上绑定的对应对象
                        TaskInfo account = param.getValue();
                        int progress = account.getProgress();
                        ProgressBar progressbar = null;
                        if (progress >= -1) {
                            // 制造 ProgressBar UI 实例
                            progressbar = new ProgressBar(progress);
                        }
                        // 绑定 progress这个property属性到UI上
//                        progressbar.progressProperty().bind(account.ObservableValue);
                        // 返回 ProgressBar 类型对象
                        return new SimpleObjectProperty<ProgressBar>(progressbar);
                    }
                });
        table.setItems(taskInfoObservableList);
    }
    @FXML
    public void deleteTask(ActionEvent event){
        TaskInfo taskInfo = (TaskInfo) table.getSelectionModel().getSelectedItem();
        if (taskInfo != null){
            taskInfoObservableList.remove(taskInfo);
        }

    }

    @FXML
    public void addRun(ActionEvent event){

        TaskInfo taskInfo = (TaskInfo) table.getSelectionModel().getSelectedItem();
        if (taskInfo != null){
            appendLog(taskInfo.getTaskName()+"=>"+"启动中...");
            taskInfo.setTaskStatus(1);
            String taskName=taskInfo.getTaskName();
            taskInfo.setProgress(-1);
            FFmpegThreadPoolExecutor.addTask(taskName,new FFmpegRunner(taskInfo));
        }
        showTableData();
    }

    @FXML
    public void removeRun(ActionEvent event){
        TaskInfo taskInfo = (TaskInfo) table.getSelectionModel().getSelectedItem();
        if (taskInfo != null){
            taskInfo.setTaskStatus(0);
            taskInfo.setProgress(-2);
            String taskName=taskInfo.getTaskName();
            FFmpegThreadPoolExecutor.earlyTermination(taskName);
        }
        showTableData();
    }

    @FXML
    public void restartRun(ActionEvent event){
        TaskInfo taskInfo = (TaskInfo) table.getSelectionModel().getSelectedItem();
        if (taskInfo != null){
            taskInfo.setTaskStatus(1);
            String taskName=taskInfo.getTaskName();
            FFmpegThreadPoolExecutor.earlyTermination(taskName);
        }
    }

    public void appendLog(String log){
        loggerTextArea.appendText(log);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showTableData();
    }
}
