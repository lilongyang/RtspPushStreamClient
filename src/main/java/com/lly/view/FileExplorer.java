package com.lly.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value="/fxml/fileExplorer.fxml",title="fileExplorer",bundle = "i18n.index",encoding = "utf-8")
public class FileExplorer extends AbstractFxmlView {
}
