# RTSPPushStreamClient-JavaFx

#### 介绍

这是为RTSP协议转发到RTMP协议的一款推流客户端，纯java所写,引用了Javacv里面的javacpp库和javaffmpeg库 ，客户端界面为javafx 所写实现基本样式和功能

#### 软件架构

JavaFx为界面 SpringBoot管理框架 JavaCV流转发库

#### 依赖说明

springboot-javafx-support 请使用 2.1.7
=> [springboot-javafx-support](https://github.com/lilongyang8/springboot-javafx-support)