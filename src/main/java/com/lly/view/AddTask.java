package com.lly.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value="/fxml/AddTask.fxml",title="AddTask",bundle = "i18n.index",encoding = "utf-8")
public class AddTask extends AbstractFxmlView {
}
