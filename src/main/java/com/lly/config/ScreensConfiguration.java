//package com.lly.config;
//
//import com.lly.controller.AddTaskController;
//import com.lly.controller.FileExplorerController;
//import javafx.event.EventHandler;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.scene.control.Alert;
//import javafx.scene.control.ButtonType;
//import javafx.stage.Stage;
//import javafx.stage.StageStyle;
//import javafx.stage.WindowEvent;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.context.annotation.Scope;
//
//import java.util.Optional;
//
//@Configuration
//@Lazy
//public class ScreensConfiguration {
//    private Stage primaryStage;
//
//    public ScreensConfiguration() {
//    }
//
//    public void setPrimaryStage(Stage primaryStage) {
//        this.primaryStage = primaryStage;
//    }
//
//    public void showScreen(Parent screen) {
//        primaryStage.setScene(new Scene(screen, 777, 500));
//        primaryStage.show();
//    }
//    @Bean
//    @Scope("singleton")
//    public FileExplorerController fileExplorerController() {
//        return new FileExplorerController();
//    }
//
//    @Bean
//    @Scope("singleton")
//    public AddTaskController addTaskController() {
//        return new AddTaskController();
//    }
//
//    @Bean
//    @Scope("singleton")
//    public FXMLDialog mainDialog(){
//        FXMLDialog dialog = new FXMLDialog(fileExplorerController(), getClass().getResource("/fxml/fileExplorer.fxml"), primaryStage);
//        dialog.setTitle("推流客户端");
//        dialog.setResizable(false);//禁止拖动
//        dialog.initStyle(StageStyle.DECORATED);//删除窗口按钮
//        dialog.setOnCloseRequest(new EventHandler<WindowEvent>() {
//            @Override
//            public void handle(WindowEvent event) {
//                //对话框 Alert Alert.AlertType.CONFIRMATION：反问对话框
//                Alert alert2 = new Alert(Alert.AlertType.CONFIRMATION);//AlertType.INFORMATION   //信息对话框
//                //AlertType.WARNING            // 警告对话框
//                //AlertType.ERROR                //错误对话框
//                //设置对话框标题
//                alert2.setTitle("Exit");
//                //设置内容
//                alert2.setHeaderText("是否确定要退出");
//                //显示对话框
//                Optional<ButtonType> result = alert2.showAndWait();
//                //如果点击OK
//                if (result.get() == ButtonType.OK){
//                    // ... user chose OK
//                    dialog.close();
//                    System.exit(1);
//                    //否则
//                } else {
//                    event.consume();
//                }
//            }
//        });
//        return dialog;
//    }
//
//
//    @Bean
//    @Scope("singleton")
//    public FXMLDialog addTaskDialog() {
//        FXMLDialog dialog = new FXMLDialog(addTaskController(), getClass().getResource("/fxml/AddTask.fxml"), primaryStage);
//        dialog.setResizable(false);//禁止拖动
//        dialog.initStyle(StageStyle.DECORATED);//删除窗口按钮
//        return dialog;
//    }
//
//
//
//
//
//
//}
