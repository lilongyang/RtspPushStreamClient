package com.lly.controller;

import com.lly.entry.TaskInfo;
import com.lly.view.AddTask;
import de.felixroske.jfxsupport.FXMLController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

@FXMLController
public class AddTaskController implements Initializable {
    @Autowired
    private FileExplorerController controller;
    @FXML
    private TextField taskNameText;
    @FXML
    private TextField inPathText;
    @FXML
    private TextField outPathText;
    @FXML
    private ToggleGroup protocolButton;
    @FXML
    private MediaView mediaView;

    private TaskInfo taskInfo;
    @Autowired
    private AddTask addTask;

    public AddTaskController() {
    }


    /**
     * 确认
     *
     * @param event
     */
    @FXML
    public void ok(ActionEvent event) {
        taskInfo = new TaskInfo(taskNameText.getText(), inPathText.getText(), outPathText.getText(), 100, "");
        taskNameText.clear();
        inPathText.clear();
        outPathText.clear();
        addTask.hide();
//        controller.showTableData();
    }

    /**
     * 取消
     *
     * @param event
     */
    @FXML
    public void cancel(ActionEvent event) {
        taskNameText.clear();
        inPathText.clear();
        outPathText.clear();
    }

    public TaskInfo getTaskInfo() {
        return taskInfo;
    }

    /**
     * 检查是否播放
     *
     * @param event
     */
    @FXML
    public void checkPlay(ActionEvent event) {
        String inPath = inPathText.getText();
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
//        String path = "/home/lenovo/Videos/dde-introduction.mp4";
//        String path1 = "http://download.oracle.com/otndocs/products/javafx/oow2010-2.flv";
//        Media media = new Media(path1);
//        MediaPlayer mplayer = new MediaPlayer(media);
//        mediaView = new MediaView(mplayer);
//        mediaView.setFitHeight(500);
//        mediaView.setFitWidth(720);
//        mplayer.setAutoPlay(true);


        try {

            FFmpegFrameGrabber grabber = FFmpegFrameGrabber.createDefault(inPath);//获取远程视频流
            grabber.setOption("rtsp_transport", "tcp"); // 使用tcp的方式，不然会丢包很严重
//        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0); //获取本机摄像头
            grabber.start();//开始获取摄像头数据
            CanvasFrame canvas = null;
            canvas = new CanvasFrame("摄像头");//新建一个窗口
            canvas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            canvas.setAlwaysOnTop(true);

            Frame frame = null;
            while (true) {
                long s = System.currentTimeMillis();
                if (!canvas.isDisplayable()) {
                    //窗口是否关闭
                    try {
                        grabber.stop();//停止抓取
                    } catch (FrameGrabber.Exception e) {
                        e.printStackTrace();
                    }
                    System.exit(1);//退出
                }
                if (frame != null) {
                    canvas.showImage(frame);//获取摄像头图像并放到窗口上显示，表示是一帧图像
                }
                long e = System.currentTimeMillis();
                System.out.println("使用" + (e - s) + "ms");

            }


        } catch (FrameGrabber.Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

}
