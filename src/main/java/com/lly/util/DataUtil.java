package com.lly.util;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.lly.entry.TaskInfo;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class DataUtil {
    static String path =  "";
    static {
        path = DataUtil.class.getClassLoader().getResource("data/data.json").getPath();
    }



    public static List<TaskInfo> read(){
        String json = FileUtil.readString(path, "UTF-8");
        if (StringUtils.isEmpty(json)){
            return new ArrayList<>();
        }
        return JSON.parseArray(json,TaskInfo.class);
    }


    public static void write(List<TaskInfo> taskInfos){
        String json = JSONUtil.toJsonStr(taskInfos);
        FileUtil.writeString(json,path, "UTF-8");
    }
}
