package com.lly.thread;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledFuture;

public class FFmpegGuardRunner implements Runnable {
    private static Logger logger = LoggerFactory.getLogger(FFmpegGuardRunner.class);
    private ScheduledFuture scheduledFuture;

    public FFmpegGuardRunner() {
    }

    public FFmpegGuardRunner(ScheduledFuture scheduledFuture) {
        if (logger.isDebugEnabled()){
            logger.debug("ffmpegRunner is init");
        }
        this.scheduledFuture = scheduledFuture;
    }

    @Override
    public void run() {
        if (logger.isDebugEnabled()){
            logger.debug("ffmpegRunner is running");
        }
        if (scheduledFuture != null){
            if (logger.isDebugEnabled()){
                logger.debug("ffmpegrunner is not null");
            }

            scheduledFuture.cancel(true);
            if (logger.isDebugEnabled()){
                logger.debug("ffmpegrunner cancel success");
            }
        }
        if (logger.isDebugEnabled()){
            logger.debug("ffmpegrunner is null");
        }
    }
}
