package com.lly;

import com.lly.thread.FFmpegThreadPoolExecutor;
import com.lly.view.FileExplorer;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PreDestroy;


/**
 * Hello world!
 */
@SpringBootApplication(scanBasePackages = "com.lly")
public class FileExplorerApplication extends AbstractJavaFxApplicationSupport {

    public static void main(String[] args) {
        launch(FileExplorerApplication.class, FileExplorer.class, args);
    }

//    @Override
//    public void start(Stage stage) throws Exception {

//        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
//        ScreensConfiguration screens = context.getBean(ScreensConfiguration.class);
//        screens.setPrimaryStage(stage);
//        screens.mainDialog().show();
//    }


    @PreDestroy
    public void destroy() {
        FFmpegThreadPoolExecutor.shutdownExecutor();
    }
}
